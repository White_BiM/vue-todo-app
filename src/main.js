import Vue from "vue";
import App from "./App.vue";
import Vuex from "vuex";
import VueRouter from "vue-router";
import VueLodash from "vue-lodash";
import lodash from "lodash";
import router from "./router";
import store from "./store";

Vue.use(Vuex, VueRouter, VueLodash, { name: "custom", lodash: lodash });
Vue.config.productionTip = false;

new Vue({
  render: h => h(App),
  router,
  store,
  created() {
    this.$store.dispatch("fetchNotes");
  }
}).$mount("#app");
