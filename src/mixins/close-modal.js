export const closeModal = {
  methods: {
    // отслеиваем Esc чтобы закрыть модяльное окно
    onKeyDown(e) {
      if (e.key === "Escape" && this.modalData) {
        this.closeModal();
      }
    }
  },
  mounted() {
    document.addEventListener("keydown", this.onKeyDown);
  }
};
