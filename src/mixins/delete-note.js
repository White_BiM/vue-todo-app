export const deleteNote = {
  methods: {
    // вызов подтверждения удаления заметки
    deleteNoteAlert(id) {
      if (id) {
        this.noteToDelete = id;
      }
      this.modalData = {
        text: "Удалить заметку?",
        confirmButtonText: "Да, удалить",
        declineButtonText: "Отменить"
      };
    }
  }
};
