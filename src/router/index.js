import Vue from "vue";
import Router from "vue-router";
import Notes from "@/pages/Notes";
import Note from "@/pages/Note";
import NewNote from "@/pages/NewNote";

Vue.use(Router);

export default new Router({
  mode: "history",
  routes: [
    { path: "/", redirect: "/notes" },
    {
      path: "/notes",
      props: true,
      name: "Notes",
      component: Notes
    },
    {
      path: "/notes/:id",
      props: true,
      name: "Note",
      component: Note
    },
    {
      path: "/notes/new",
      props: true,
      name: "NewNote",
      component: NewNote
    }
  ],
  scrollBehavior() {
    return { x: 0, y: 0 };
  }
});
