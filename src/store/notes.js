const mockNotes = [
  {
    id: "1",
    title: "Заметка 1",
    todoList: [
      {
        id: "1",
        text: "Read smth",
        done: false
      },
      {
        id: "2",
        text: "Drink coffee",
        done: true
      },
      {
        id: "3",
        text: "Sleep",
        done: false
      }
    ]
  },
  {
    id: "3",
    title: "Заметка 3",
    todoList: [
      {
        id: "1",
        text: "Code",
        done: false
      },
      {
        id: "2",
        text: "Eat",
        done: true
      },
      {
        id: "3",
        text: "Sleep",
        done: false
      },
      {
        id: "4",
        text: "Repeat",
        done: false
      }
    ]
  },
  {
    id: "2",
    title: "Заметка 2",
    todoList: [
      {
        id: "1",
        text: "Code",
        done: false
      },
      {
        id: "2",
        text: "Eat",
        done: true
      },
      {
        id: "3",
        text: "Sleep",
        done: false
      },
      {
        id: "4",
        text: "Repeat",
        done: false
      }
    ]
  }
];
Storage.prototype.setObj = function(key, obj) {
  return this.setItem(key, JSON.stringify(obj));
};
Storage.prototype.getObj = function(key) {
  return JSON.parse(this.getItem(key));
};

export default {
  state: {
    notes: []
  },
  mutations: {
    setNotes(state, payload) {
      state.notes = payload;
    },
    saveNote(state, { id, title, todoList }) {
      let newNote = state.notes.find(note => note.id === id);
      if (newNote) {
        newNote.title = title;
        newNote.todoList = todoList;
      } else {
        const note = {
          id,
          title,
          todoList
        };
        state.notes.push(note);
      }
      localStorage.setObj("VueTodoAppNotes", state.notes);
    },
    deleteNote(state, noteId) {
      const myArray = state.notes.filter(function(obj) {
        return obj.id !== noteId;
      });
      state.notes = myArray;
      localStorage.setObj("VueTodoAppNotes", state.notes);
    }
  },

  actions: {
    fetchNotes({ commit }) {
      let notesArr = [];
      if (localStorage.getItem("VueTodoAppNotes")) {
        notesArr = JSON.parse(localStorage.getItem("VueTodoAppNotes"));
      } else {
        notesArr = mockNotes;
      }
      commit("setNotes", notesArr);
    },
    saveNote({ commit }, note) {
      try {
        commit("saveNote", note);
      } catch (e) {
        console.error(e);
        throw e;
      }
    },
    deleteNote({ commit }, noteId) {
      try {
        commit("deleteNote", noteId);
      } catch (e) {
        console.error(e);
        throw e;
      }
    }
  },

  getters: {
    notes(state) {
      return state.notes;
    },
    noteById(state) {
      return noteId => {
        return state.notes.find(note => note.id === noteId);
      };
    }
  }
};
